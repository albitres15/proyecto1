<?php
require_once("includes/header.php");
?>

<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

<!-- creadndo table de la lista-->

<div class="container-fluid">


    <div class="  container-fluid col-xl-8 offset-xl-2 div_body">
    <div id="alerta_message" class="mt-4"></div>
        <section id="Formulario_insert_update" class=" mt-4">
            <div class=" container col-xl-6 col-lg-6 mt-3 mb-4">
           

                <form action="" method="POST" enctype="multipart/form-data">

                    <div class="form-group">

                        <input type="text" id="nombres" name="nombres" class="form-control" id="validationServer01"
                             minlength="1" maxlength="50" placeholder="Ingrese Nombre"
                            onkeyup="this.value=NumText(this.value)" autofocus required>
                        <div class="valid-tooltip">
                            Looks good!
                        </div>

                    </div>
                    <div class="form-group">
                        <input type="text" id="apellidos" name="apellidos" class="form-control"
                            placeholder="Ingrese Apellido"  minlength="1" maxlength="50"
                            onkeyup="this.value=NumText(this.value)" required>

                    </div>
                    <div class="form-group">
                        <input type="number" id="edad" name="edad" class="form-control" placeholder="Ingrese Edad"
                             min="5" max="50" onkeyup="this.value=Numeros(this.value)" required>

                    </div>
                    <div class="form-group">
                        <input type="number" id="dni" name="dni"  min="30000000" max="99999999"
                            class="form-control" onkeyup="this.value=Numeros(this.value)" placeholder="Ingrese Dni" required>

                    </div>
                    <div class="form-group">
                    <input type="date" id="fecha_de_nacmiento" name="party"  min="1936-01-01"  class="form-control"  max="2025-12-30" required>

                    </div>
                    <div class="form-group">
                        <input type="text"  id="Aula" name="Aula" class="form-control" placeholder="Aula" 
                            min="5" max="41" minlength="1" maxlength="5" required>
                   
                    </div >

                    <div class="form-group">
                        <input type="file" name="imagen" id="imagen1" class="imagen">
                    </div>
                    <input type="hidden" id="id" name="id">
                    <!--dato oculto para poder ejecutar una funcion-->

                    <div>
                    <input type="submit" class="btn btn-primary  ml-4 col-5"  name="Insertar" id="Insertar" value="Insertar" onclick="desparecer_form_llenado()">
                    <input type="button"  class="btn btn-secondary ml-4 col-5" name="Cancelar" id="Cancelar"  value="Cancelar" onclick="desaparecer()">
                    </div>
                </form>

            </div>
        </section>
        <section id="tabla" class="mt-4">

            <div class="container ">
                <table class="table  tabla_alumno ">

                
                    <!--creando cabecera de tablla-->
                    <thead class="thead-dark text-center abs-center">
                        <tr>
                            <th scope="col">Orden Alumnos</th>

                            <th class=" d-none d-sm-table-cell">Nombres</th>

                            <th scope="col ">Apellidos</th>
                            <th scope="col">Edad</th>
                            <th scope="col">DNI</th>
                            <th scope="col" class=" d-none d-sm-table-cell">Fecha de nacimiento</th>
                            <th scope="col">Aula</th>
                            <th scope="col-2">Funciones</th>
                        </tr>
                    </thead>
                    <!--creando cuerpo de tabla-->
                    <tbody>
                        <!--trayendo datos tabla-->
                        <?php
                        /* exmaminando si es dato que trae esta vadio con un if*/
                        //if(!empty($datos_alumno)){
                        /*  Trayeendo los nombres prinpicipal  de array de bd*/
                        /*foreach ($datos_alumno as $tr_key => $valor_o_el_dato_del_key_tr){
                        foreach($valor_o_el_dato_del_key_tr  as $td_valor ){?>
                        <tr>
                        <td>
                        <?php echo $td_valor["id_de_alumno"] ?>
                        </td>
                        <td>
                        <?php echo $td_valor["nombres"] ?>
                        </td>
                        <td>
                        <?php echo $td_valor["apellidos"] ?>
                        </td>
                        <td>
                        <?php echo $td_valor["edad"] ?>
                        </td>
                        <td>
                        <?php echo $td_valor["dni"] ?>
                        </td>
                        <td>
                        <?php echo $td_valor["fecha_de_nacmiento"] ?>
                        </td>
                        <td>
                        <?php echo $td_valor["Aula"] ?>
                        </td>
                        <td>
                        <a href="index.php?m=vistaloadupdate&id_de_alumno=<?php echo $td_valor["id_de_alumno"] ?>" class="btn  btn-warning">
                        <i class="fa fa-pencil-square" aria-hidden="true"></i></i>
                        </a>
                        <a href="index.php?m=delete&id_de_alumno=<?php echo $td_valor["id_de_alumno"] ?>  " class="btn  btn-danger">
                        <i class="fa-solid fa-trash"></i>
                        </a>
                        
                        </td>
                        </tr>
                        <?php }  ?>
                        <?php } } else{ echo ("No hay  Registros");} */?>
                         <?php echo('<pre>'); print_r($_FILES); echo('</pre>') ?>
                    </tbody>
                    
                </table>
                <div class="container ">
                    <div id="pagination-wrapper"></div>
                </div>


                
       
                
            </div>
        </section>
    </div>
</div>
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Modal title</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        Estas seguro que quieres elimiar al Alumno
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" data-dismiss="modal">Cancelar</button>
                        <button type="button"  id="btn_eliminar_confirmar" name="btn_eliminar_confirmar" onclick="actividad_eliminar()"  class="btn btn-danger" data-dismiss="modal">Eliminar</button>
                            
                    </div>
                    </div>
                </div>
                </div>





<?php
require_once("includes/footer.php");
?>